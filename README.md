SearchApp
===========================
Opis
-------------------------
Zadanie rekrutacyjne na JS deva w firmie Esri Polska. Treść zadania znajduje się w pliku OpisZadania.pdf. Projekt graficzny widgetu znajduje się w pliku widget.png. Autorem treści zadania i projektu widgetu jest Pan Adam Kuran.

Instrukcja uruchomienia
-------------------------

1. W pierwszej kolejności trzeba zainstalować paczki npm z pliku package.json poprzez komendę npm install. 
2. Następnie jeśli nie ma zainstalowanych globalnie paczek gulp i json-server trzeba je zainstalować (npm install --global gulp-cli oraz npm install -g json-server). 
3. Następnie, jeśli chce się lokalnie uruchomić projekt należy zrobić 2 rzeczy:
4. Z powodu zabezpieczenia typu CORS, plik JSON nie może być odczytany bezpośrednio z dysku i należy go "zahostować". W konsoli trzeba zatem przejść do podfolderu ../build/json (uruchomienie konsoli w folderze projektu i następnie komendy: cd build oraz cd json) i będąc w tym miejscu należy wpisać komendę: 'json-server --watch config.json'. Pozwoli ona na nasłuchiwanie na plik config.json. Domyślnie plik dostępny jest pod adresem http://localhost:3000. Jeśli port byłby inny niż 3000 należy w pliku config.js w ostatniej linijce dokonać zmiany na odpowiedni.
5. Aby odpalić projekt wystarczy teraz w folderze build włączyć plik index.html. W celu dokonania jakichś zmian łatwiej będzie pracować na plikach źródłowych w folderze src. Aby robić to w najwygodniejszy sposób należy w folderze z projektem wpisać polecenie gulp. Wtedy każda zmiana w plikach źródłowych będzie miała odzwierciedlenie w plikach w folderze build. Projekt domyślnie można znaleźć pod adresem http://localhost:3001/
6. Aby wrzucić pliki na jakiś serwer należy wtedy zmienić link adresu z którego będą pobierane ustawienia z pliku JSON. Należy wtedy w pliku config.js w ostatniej linijce podmienić ścieżkę 'http://localhost:3000/config' na odpowiednią. 
7. Modyfikację adresu serwisu, etykiet przed polami wyszukiwania oraz nazwy pól wykorzystywanych do wyszukiwania można przeprowadzić w pliku config.json.
8. Odpowiedź na ostatnie pytanie umieściłem w pliku answer.txt