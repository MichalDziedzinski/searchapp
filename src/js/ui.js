class UI {
    constructor() {
        //UI selectors of HTML DOM
        this.UISelectors = {
            inputOne: 'input-one',
            inputTwo: 'input-two',
            btnSearch: 'btn-search',
            btnClear: 'btn-clear',
            results: '.results-js',
            table: '.results-table-js',
            formGroupOne: 'form-group-1',
            formGroupTwo: 'form-group-2',
            map: '.map-js',
            resultsParagraph: '.results__paragraph-js',
            countParagraph: 'count'
        };
    }

    //Show search results
    showResults(results) {
        //Checking if there are any search results
        if (results.length > 0) {

            //Clear alert that searching is in progress
            this.clearAlert();
            //Clearthe previous search
            this.clearResults();

            //Create results view
            const resultText = document.createTextNode('Wyniki:');

            document.querySelector(this.UISelectors.resultsParagraph).appendChild(resultText);

            //Creating a result table
            let output = `
            <tr class="results-table__row">
                <th class="results-table__cell results-table__cell--first results-table__cell--header">Ulica</th>
                <th class="results-table__cell results-table__cell--second results-table__cell--header">Numer</th>
                <th class="results-table__cell results-table__cell--third results-table__cell--header">Przybliż</th>
            </tr>`;
            results.forEach((result) => {
                output += `
                <tr class="results-table__row">
                    <td class="results-table__cell results-table__cell--first">${result.attributes.ulica}</td>
                    <td class="results-table__cell results-table__cell--second">${result.attributes.numerPorza}</td> 
                    <td class="results-table__cell results-table__cell--third"><a class="results-table__link" href="https://wego.here.com/directions/mix//ulica-${result.attributes.ulica}-${result.attributes.numerPorza},-27-215-Wąchock,-Polska" target="_blank">Przybliż</a></td>
                </tr>
                `;
            });

            document.querySelector(this.UISelectors.table).innerHTML = output;
        }
    }

    //Clear inputs values
    clearInputs() {
        document.getElementById(this.UISelectors.inputOne).value = '';
        document.getElementById(this.UISelectors.inputTwo).value = '';
    }

    //Clear search results
    clearResults() {
        //Clear table content
        document.querySelector(this.UISelectors.table).innerHTML = '';
        //Clear paragraphs content
        document.querySelector(this.UISelectors.resultsParagraph).innerHTML = '';
        document.getElementById(this.UISelectors.countParagraph).innerHTML = '';
    }

    //Change map iframe src
    changeMap(href) {
        document.querySelector(this.UISelectors.map).setAttribute('src', href)
    }

    //Set input labels for those from a file
    changeLabels(labelOne, labelTwo) {
        document.getElementById(this.UISelectors.inputOne).previousElementSibling.innerHTML = `${labelOne}:`;
        document.getElementById(this.UISelectors.inputTwo).previousElementSibling.innerHTML = `${labelTwo}:`;
    }

    //Show amount of results of searching
    showCount(count) {
        //Checking if the number of search results is greater than 0
        if (count > 0) {
            //Display how many search results there are
            const countText = document.createTextNode(`Liczba wyszukanych adresów: ${count}`);
            document.getElementById(this.UISelectors.countParagraph).appendChild(countText);
        } else {
            //Alert that there are no search results
            this.showAlert('Brak wyników wyszukiwania', 'alert alert--bad')
        }
    }

    // Show alert message
    showAlert(message, className) {
        this.clearAlert();

        // Create div
        const div = document.createElement('div');
        // Add classes
        div.className = className;
        // Add text
        div.appendChild(document.createTextNode(message));
        // Get parent
        const container = document.querySelector(this.UISelectors.results);
        // Get paragraph
        const paragraph = document.querySelector(this.UISelectors.resultsParagraph);
        // Insert alert div
        container.insertBefore(div, paragraph);

        // Timeout
        setTimeout(() => {
            this.clearAlert();
        }, 3000);
    }

    // Clear alert message
    clearAlert() {
        const currentAlert = document.querySelector('.alert');

        if (currentAlert) {
            currentAlert.remove();
        }
    }
}

export const ui = new UI();