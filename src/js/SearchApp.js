//Import from other js files
import { ui } from './ui';
import { http } from './http';
import { config } from './config';
import { resultsCount } from './ResultsCount';

//Getting Config Settings from JSON
document.addEventListener('DOMContentLoaded', getConfigSettings);

//Searching adresses
document.getElementById(ui.UISelectors.btnSearch).addEventListener('click', search);

//Clear inputs and search results
document.getElementById(ui.UISelectors.btnClear).addEventListener('click', clear);

//Detecting click at href
document.querySelector(ui.UISelectors.table).addEventListener('click', selectHref);


function search(e){
    //Get values from inputs and trimming white signs
    const inputOne = document.getElementById(ui.UISelectors.inputOne).value.trim();
    const inputTwo = document.getElementById(ui.UISelectors.inputTwo).value.trim();
    //Check that inputs are not empty
    if(inputOne === '' || inputTwo === ''){
        //Show alert that at least one input is empty
        ui.showAlert('Uzupełnij oba pola', 'alert alert--bad');
    } else {
        //Show alert that searching is in progress
        ui.showAlert('Trwa wyszukiwanie...', 'alert alert--good');

        //Getting data from service, and send them to display in table and to count amount of them
        http.get(`${config.serviceAddress}/query?where=${config.queryParameterOne}%27${inputOne}%27+and+${config.queryParameterTwo}+%27${inputTwo}%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=none&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=ulica%2CnumerPorza&returnGeometry=false&multipatchOption=none&maxAllowableOffset=&geometryPrecision=&outSR=&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnDistinctValues=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=true&quantizationParameters=&sqlFormat=none&f=pjson&token=`)
        .then(data => {
            ui.showResults(data.features);
            resultsCount.counter(data.features);
        })
        .then(() => (ui.showCount(resultsCount.count)))
        .catch(err => console.log(err));
    }

    //Prevent default behavior of event
    e.preventDefault();
};

function clear(e){
    //Clear values from inputs
    ui.clearInputs();
    //Clear search results
    ui.clearResults();
    //Alarm that clear is completed
    ui.showAlert('Usunięto wyniki wyszukiwania', 'alert alert--good');

    //Prevent default behavior of event
    e.preventDefault();
};

function selectHref(e){
    //Checking if the link was clicked
    if(e.target.classList.contains('results-table__link')){
        //Change map iframe src
        ui.changeMap(e.target.getAttribute('href'));
    }

    //Prevent default behavior of event
    e.preventDefault();
}

function getConfigSettings(){
    //Getting config setting from JSON and send them to config object and to change the UI
    http.get('http://localhost:3000/config')
    .then(data => config.setConfigSettings(data))
    .then(() => ui.changeLabels(config.labelOne, config.labelTwo))
    .catch(err => console.log(err));
}