class Config {
    constructor(link) {
        this.link = link;
        this.labelOne;
        this.labelTwo;
        this.serviceAddress;
        this.queryParameterOne;
        this.queryParameterTwo;
    }
    
    setConfigSettings(data){
        //Set config settings to object properties
        this.labelOne = data[0].labelOne;
        this.labelTwo = data[0].labelTwo;
        this.serviceAddress = data[0].serviceAddress;
        this.queryParameterOne = data[0].queryParameterOne;
        this.queryParameterTwo = data[0].queryParameterTwo;
    }
}

//At this point, the address should be changed if it changes
export const config = new Config('http://localhost:3000/config');